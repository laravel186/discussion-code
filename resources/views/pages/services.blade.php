<!-- <html>
    <head>
        <title>Services</title>
    </head>
    <body>
        <h1>Services</h1>
        <p>This is the Services Section</p>
    </body>
</html> -->
@extends('layouts.app')

@section('content')
    <h1>Services</h1>
    <p>This is the Services Section</p>
    @if(count($services) > 0)
        @foreach($services as $service)
            <li>{{ $service }}</li>
        @endforeach
    @endif
@endsection