<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

/* Route::get('/',function(){
    return view ('welcome.blade.php');
}); */

/* Route::get('/hello', function () {
    return view('hello');
}); */

Route::get('/hello', [PageController::class,'hello']);
Route::get('/hello1', [PageController::class,'hello1']);
Route::get('/services', [PageController::class,'services']);
Route::get('/about', [PageController::class,'about']);
Route::resource('/posts', PostController::class);
Route::get('/posts/create', [PostController::class,'create']);
Route::get('/', [PageController::class,'index']);
Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');
