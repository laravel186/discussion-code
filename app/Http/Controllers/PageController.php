<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function hello(){
        $info = array(
            'frontend' => 'Zuitt Coding Bootcamp',
            'topics' => ['HTML and CSS', 'JS DOM', 'React'],
        );
        // return view('hello')->with('name','Homer Simpson');
        return view('hello')->with($info);
    }

    // public function hello1(){
    //     return view('hello',['name'=>'Homer Simpson']);
    // }

    public function services(){
        $serviceArr = array(
            'services' => ['Web Design', 'Development', 'SEO'],
        );
        return view('/pages/services')->with($serviceArr);
    }
    
    public function about(){
        return view('/pages/about');
    }

    public function index(){
        return view('/pages/index');
    }
}
